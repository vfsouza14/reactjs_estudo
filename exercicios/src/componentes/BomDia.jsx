//import React, { Fragment } from 'react'
import React from 'react'

/* export default props =>
    <div>
        <h1>Bom dia {props.nome}</h1>
        <h2>Idade {props.idade}</h2>
    </div> 
     <React.Fragment>
        <h1>Bom dia {props.nome}</h1>
        <h2>Idade {props.idade}</h2>
    </React.Fragment> 

    <Fragment>
        <h1>Bom dia {props.nome}</h1>
        <h2>Idade {props.idade}</h2>
    </Fragment> */
    
    export default props => [
        <h1 key='h1'>Bom dia {props.nome}</h1>,
        <h2 key='h2'>Idade {props.idade}</h2>
    ]