import React from 'react'
import ReactDOM from 'react-dom'
//import { BoaNoite, BoaTarde } from './componentes/Multiplos'
//import Primeiro from './componentes/Primeiro'
//import BomDia from './componentes/BomDia'

//import { BoaTarde, BoaNoite } from './componentes/Multiplos'
//import Multi, { BoaNoite } from './componentes/Multiplos'

//import Saudacao from './componentes/Saudacao'

import Pai from './componentes/Pai'
import Filho from './componentes/Filho'


/* ReactDOM.render(
    <div>
        <Multi.BoaTarde nome="Ana" />
        <BoaNoite nome="Bia" />
    </div>, 
document.getElementById('root')) */

/* ReactDOM.render(
    <div>
        <Saudacao tipo="Bom dia " nome="João"/>
    </div>, 
document.getElementById('root')) */

ReactDOM.render(
    <div>
        <Pai nome="Gilson " sobrenome="de Souza">
            <Filho nome="Pedro"  />
            <Filho nome="Victor"  />
            <Filho nome="Carla"  />
        </Pai>
    </div>, 
document.getElementById('root'))